# README #

This README contains specific information to launch CLIPS rules engine on Android platform

### Essential files ###

Before launching the app, you need to load the .clp file with a model into phone/tablet memory. By default, the file must be in the internal storage (not on the memory card) and must be placed inside "clips" folder. The sample file is available in the "Downloads" section.