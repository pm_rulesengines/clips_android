package pl.me.clipstest;

import android.app.Activity;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.os.Debug;
import android.text.method.ScrollingMovementMethod;
//import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import eu.deustotech.clips.CLIPSError;
import eu.deustotech.clips.Environment;
import eu.deustotech.clips.IntegerValue;
import eu.deustotech.clips.PrimitiveValue;
//import net.sf.clipsrules.jni.CLIPSError;
//import net.sf.clipsrules.jni.Environment;
//import net.sf.clipsrules.jni.FactAddressValue;
//import net.sf.clipsrules.jni.IntegerValue;
//import net.sf.clipsrules.jni.MultifieldValue;
//import net.sf.clipsrules.jni.PrimitiveValue;


public class MainActivity extends Activity {

    private static String FILE_NAME;
    private static final String PATH = android.os.Environment.getExternalStorageDirectory().getAbsolutePath()+"/clips/";
    private TextView textView;
    private RadioGroup radioGroup;
    private boolean onlyTest;
    private static boolean ifContinue = true;
    private Date start, end;
    private BufferedWriter bw;
    private long minMemory = -1;
    private long maxMemory = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        maxMemory = Debug.getNativeHeapAllocatedSize();
        minMemory = maxMemory;
        textView = (TextView) findViewById(R.id.textview);
        textView.setMovementMethod(new ScrollingMovementMethod());
        textView.setKeepScreenOn(true);
        radioGroup = (RadioGroup) findViewById(R.id.grupa);
        File clipsBaseDir = new File(PATH);
        for (File model : clipsBaseDir.listFiles()) {
            if (!model.getName().endsWith(".clp")) continue;
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(model.getName());
            radioGroup.addView(radioButton);
        }
    }

    /**
     * Method testing complete running of CLIPS engine
     * @param view
     */
    public void go (View view) {
        try {
            bw = new BufferedWriter(new FileWriter(android.os.Environment.getExternalStorageDirectory()
                    .getAbsolutePath()+"/benchmark/clips.txt",true));
            int radioButtonID = radioGroup.getCheckedRadioButtonId();
            View radioButton = radioGroup.findViewById(radioButtonID);
            int idx = radioGroup.indexOfChild(radioButton);
            RadioButton r = (RadioButton)  radioGroup.getChildAt(idx);
            FILE_NAME = r.getText().toString();
            onlyTest = false;

            String factsFilePath;
            CLIPSTest test;
            factsFilePath = PATH + FILE_NAME;

            //measuring block
            ifContinue = true;
            new Monitor().execute();
            start = new Date();
            //measuring block

            // And now, a little more complex usage...
            test = new CLIPSTest(factsFilePath);
            String res = test.getAllFacts();

            //finishing block
            end = new Date();
            ifContinue = false;
            //finishing block

            textView.setText(res);
            test.stop();

            radioGroup.setVisibility(View.GONE);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r2 = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r2.play();

        } catch (CLIPSError clipsError) {
            clipsError.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method testing only inference process of CLIPS engine
     * @param view
     */
    public void goOnlyRun(View view) {
        try {
            bw = new BufferedWriter(new FileWriter(android.os.Environment.getExternalStorageDirectory()
                    .getAbsolutePath()+"/benchmark/clips.txt",true));
            int radioButtonID = radioGroup.getCheckedRadioButtonId();
            View radioButton = radioGroup.findViewById(radioButtonID);
            int idx = radioGroup.indexOfChild(radioButton);
            RadioButton r = (RadioButton)  radioGroup.getChildAt(idx);
            FILE_NAME = r.getText().toString();
            onlyTest = true;
            String factsFilePath;
            CLIPSTest test;
            factsFilePath = PATH + FILE_NAME;

            // And now, a little more complex usage...
            test = new CLIPSTest(factsFilePath);

            //measuring block
            ifContinue = true;
            new Monitor().execute();
            start = new Date();
            //measuring block

            String res = test.getAllFacts();

            //finishing block
            end = new Date();
            ifContinue = false;
            //finishing block

            textView.setText(res);
            test.stop();

            radioGroup.setVisibility(View.GONE);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r2 = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r2.play();

        } catch (CLIPSError clipsError) {
            clipsError.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String printDateTime() {
        Calendar c = Calendar.getInstance();
        return String.format("%tY-%tm-%td %tH:%tM:%tS : ", c, c, c, c, c, c);
    }

    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception ignored) {}

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }

    public class Monitor extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            textView.setKeepScreenOn(true);
            while (ifContinue) {
//                processorUsage.add(readUsage());
                long tempMem = Debug.getNativeHeapAllocatedSize();
                if (tempMem > maxMemory) maxMemory = tempMem;
                else if (tempMem < minMemory) minMemory = tempMem;
            }
            System.out.println("MONITOR: KONIEC");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
//            Float [] proc = processorUsage.toArray(new Float[processorUsage.size()]);
//            float minProc = proc[0];
//            float maxProc = proc[0];
//            for (float p : proc) {
//                if (p < minProc) minProc = p;
//                if (p > maxProc) maxProc = p;
//            }
            try {
                bw.write(printDateTime());
                bw.write("("+FILE_NAME+") ");
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMemory - minMemory) / 1024L));
//                bw.write(" KB , Proc diff: ");
//                bw.write(String.valueOf( (maxProc-minProc)*100));
                bw.write(" KB , Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms "+(onlyTest ? "(tylko wyk)" : "")+" \n-------\n");
                bw.close();

            } catch (NullPointerException e) {
                System.err.println("Result file is not open!");
            }catch (IOException e) {
                System.err.println("IO Exception: "+e.getMessage());
            } finally {
                textView.setKeepScreenOn(false);
            }
        }
    }
}
