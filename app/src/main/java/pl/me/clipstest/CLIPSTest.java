package pl.me.clipstest;

//import android.util.Log;

//import net.sf.clipsrules.jni.Environment;

//import net.sf.clipsrules.jni.CLIPSError;
//import net.sf.clipsrules.jni.CLIPSError;
//import net.sf.clipsrules.jni.Environment;
//import net.sf.clipsrules.jni.FactAddressValue;
//import net.sf.clipsrules.jni.IntegerValue;
//import net.sf.clipsrules.jni.MultifieldValue;

import eu.deustotech.clips.CLIPSError;
import eu.deustotech.clips.Environment;
import eu.deustotech.clips.FactAddressValue;
import eu.deustotech.clips.FloatValue;
import eu.deustotech.clips.MultifieldValue;
import eu.deustotech.clips.SymbolValue;

/*
 * This class is based on an introductory example on how to use CLIPSJNI made by C. Daniel Sánchez Ramírez.
 *   https://bitbucket.org/ErunamoJAZZ/trash-it/src/94282935833e4271065787ba75d02a7ce6ff9b24/docs/Como%20Utilizar%20CPLISJNI.pdf?at=default
 */
public class CLIPSTest {

    public static final String tag = "CLIPS Rules Engine";
    private final Environment clips;

    public CLIPSTest( String filepath ) throws CLIPSError {
        this.clips = new Environment();
        this.clips.load(filepath);
//        Log.d(CLIPSTest.tag, "Loading .clp...\n\n");
        this.clips.reset();
    }

    public void stop() {
        this.clips.destroy();
    }

    private String showAttribute(FactAddressValue personFact) {
        String attr = personFact.getFactSlot("name").toString();
        double floatValue = 0.0;
        String stringValue;
        try {
            stringValue = ((SymbolValue) personFact.getFactSlot("value")).symbolValue();
        } catch (NumberFormatException | ClassCastException e) {
            stringValue = null;
            floatValue =((FloatValue) personFact.getFactSlot("value")).floatValue();
        }
//        Log.d(CLIPSTest.tag, name + " is "+ age + " old.");
        return attr + " = "+ (stringValue != null ? stringValue : floatValue);
    }

//    /**
//     * Example of how to use "assert".
//     * @throws CLIPSError
//     */
//    public void assertExample() throws CLIPSError {
//        final FactAddressValue Sandra = this.clips.assertString("(attribute (name xattr_2) (value cd))");
////        showAttribute( Sandra );
//    }

    /**
     * Example of how to get data using "eval".
     * @throws CLIPSError
     */
    public String getAllFacts() throws CLIPSError {
        clips.run(); //is required to launch inference process
        final String evalStr = "(find-all-facts (( ?f attribute )) TRUE)";
        final MultifieldValue evaluated = (MultifieldValue) this.clips.eval( evalStr );
        return showKnowledgeState(evaluated);
    }

//    /**
//     * Changing a fact using a rule defined in the original file.
//     * @throws CLIPSError
//     */
//    public void modifyAFact() throws CLIPSError {
//        this.clips.assertString("(birthday Ana)");
//        this.clips.run();
//    }

    private String showKnowledgeState(MultifieldValue evaluated) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i < evaluated.size(); i++) {
            final FactAddressValue person = (FactAddressValue) evaluated.get(i);
            sb.append(showAttribute(person)).append("\n");
//            showAttribute( person );
        }
        return sb.toString();
    }
}